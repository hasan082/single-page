import 'package:flutter/material.dart';

void main() {
  runApp(const ShoppingListApp());
}

class ShoppingListApp extends StatelessWidget {
  const ShoppingListApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ShoppingListScreen(),
    );
  }
}

class ShoppingListScreen extends StatelessWidget {
  ShoppingListScreen({super.key});

  final List<String> shoppingItems = [
    'Shop Item 1',
    'Shop Item 2',
    'Shop Item 3',
    'Shop Item 4',
    'Shop Item 5',
  ];

  final List<IconData> shoppingIcon = [
    Icons.shopping_basket,
    Icons.shopping_cart,
    Icons.shopping_bag,
    Icons.shop_2,
    Icons.shopping_basket_outlined
  ];

  Widget shoppingItemWidget (IconData icon, String item){
    return  ListTile(
      leading: Icon(icon),
      title: Text(item, style: const TextStyle(fontSize: 20),),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Shopping List'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: const Icon(Icons.shopping_cart),
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Cart is empty')),
              );
            },
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: shoppingItems.length,
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (BuildContext context, int index) {
          return shoppingItemWidget(shoppingIcon[index], shoppingItems[index]);
        },
      ),
    );
  }
}
